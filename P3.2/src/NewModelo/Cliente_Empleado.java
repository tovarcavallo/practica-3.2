/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewModelo;




/**
 *
 * @author carlo
 */
public class Cliente_Empleado {
    int idEmpleado;
    int idCliente;
   

    public Cliente_Empleado(int idEmpleado, int idCliente) {
        this.idEmpleado = idEmpleado;
        this.idCliente = idCliente;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    @Override
    public String toString() {
        return "Cliente_Empleado{" + "idEmpleado=" + idEmpleado + ", idCliente=" + idCliente + '}';
    }
    
}
