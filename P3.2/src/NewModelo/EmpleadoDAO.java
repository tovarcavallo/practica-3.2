/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewModelo;

import java.util.List;

/**
 *
 * @author carlo
 */
public interface EmpleadoDAO {
    public List<Empleado> listarCEmpleado(int desde, int cuanto);
    public boolean insertEmpleado(String nombre);
    public boolean updateEmpleado(Empleado empleado);
    public boolean deleteEmpleado(int idEmpleado);
    public Empleado getEmpleado(int idEmpleado);
    public List<Empleado> getEmpleadoLike(String condicion);
    
}
