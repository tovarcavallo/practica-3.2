/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NewModelo;

import java.util.List;

/**
 *
 * @author carlo
 */
public interface ClienteDAO {
    public List<Cliente> listarClientes(int desde, int cuanto);
    public boolean insertCliente(String nombre);
    public boolean updateCliente(Cliente cliente);
    public boolean deleteCliente(int idCliente);
    public Cliente getCliente(int idCliente);
    public List<Cliente> getClienteLike(String condicion);
    
}
