
package Test;

import Modelo.Conexion;
import NewModelo.Cliente;
import NewModelo.Cliente_Empleado;
import NewModelo.Empleado;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Pruebas {

    public static void main(String[] args) throws IOException, SQLException {
     Conexion con = new Conexion();
        Connection cone = con.getConnection();
        Statement st = cone.createStatement();
        
     Cliente maria = new Cliente("Maria","Sandobal","Calle 3","0000000");
        System.out.println(maria);
     Cliente Carlos = new Cliente("Carlos","Sandobal","Calle 3","0000099");
        System.out.println(Carlos);
     Empleado Pepe = new Empleado("Pepe","Ramirez","Calle 4","098765432");
        System.out.println(Pepe);
     Empleado Lola = new Empleado("Lola","Gutierrez","Calle 9","098778432");
        System.out.println(Lola);
     Cliente_Empleado p = new Cliente_Empleado(1,2);
     System.out.println(p);
     Cliente_Empleado p1 = new Cliente_Empleado(2,1);
     System.out.println(p1);
     
     
     
     
        Conexion.closeConnection(cone);
    }
    
}
